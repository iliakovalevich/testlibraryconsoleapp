package com.intexsoft;

import com.intexsoft.services.io.ConsoleService;
import com.intexsoft.services.book.BookService;
import com.intexsoft.services.book.IBookService;
import com.intexsoft.services.io.IConsoleService;

public class Main {

    public static void main(String[] args) {
        try {
            startConsole();
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }

    private static void startConsole() {
        IConsoleService consoleService = new ConsoleService();
        IBookService bookService = new BookService();
        while (true) {
            switch (consoleService.getMenuNumber()) {
                case "1":
                    consoleService.printHashMapValues(bookService.readAll());
                    break;
                case "2":
                    bookService.create(consoleService.createNewBook());
                    break;
                case "3":
                    consoleService.printString(bookService.getById(consoleService.getBookId()).toString());
                    break;
                case "0":
                    return;
                default:
                    consoleService.printString("Your input is incorrect, please try again");
                    break;
            }
        }
    }
}
