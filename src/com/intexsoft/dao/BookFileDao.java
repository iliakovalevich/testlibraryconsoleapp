package com.intexsoft.dao;

import com.intexsoft.entities.Book;

import java.io.*;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public class BookFileDao implements IBookFileDao {
    private static final String PATH_TO_FILE_WITH_BOOKS = "books.properties";

    @Override
    public void create(Book entity) {
        Properties properties = readFromFile();
        String value = entity.getTitle() + ";" + entity.getAuthor();
        properties.put(entity.getId(), value);
        writeToFile(properties);
    }

    @Override
    public Map<String, String> readAll() {
        Map<String, String> hashMap = new HashMap<>();
        Properties properties = readFromFile();
        for (Object key : properties.keySet()) {
            hashMap.put(key.toString(), properties.getProperty(key.toString()));
        }
        return hashMap;
    }

    @Override
    public void update(Book entity) {

    }

    @Override
    public void delete(Integer id) {

    }

    private Properties readFromFile() {
        Properties properties = new Properties();
        try (InputStream inputStream = BookFileDao.class.getClassLoader().getResourceAsStream(PATH_TO_FILE_WITH_BOOKS)) {
            properties.load(inputStream);
        } catch (IOException ioException) {
            ioException.printStackTrace();
        }
        return properties;
    }

    private void writeToFile(Properties properties) {
        try (FileOutputStream out = new FileOutputStream(new File(getClass().getClassLoader().getResource(PATH_TO_FILE_WITH_BOOKS).toURI()))) {
            properties.store(out, null);
        } catch (URISyntaxException | IOException e) {
            e.printStackTrace();
        }
    }
}
