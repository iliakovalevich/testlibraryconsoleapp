package com.intexsoft.dao;


import com.intexsoft.entities.Book;

import java.util.Map;


/**
 * Интерфес для работы с файлами
 * Основные методы интерфейса(Создать запись в файле, прочитать всё из файла, изменить поле файла, удалить поле из файла)
 *
 * @version 1.0
 */
public interface IBookFileDao {

    /**
     * Метод добавления новой сущности в файл.
     *
     * @param entity - сущность , которую записываем в файл
     */
    void create(Book entity);

    /**
     * Метод, который возвращает Map в которой находятся ключи(.property файла) и значения (.property файла)
     *
     * @return Map<String, String>
     */
    Map<String, String> readAll();

    /**
     * Метод изменения поля в property файле
     *
     * @param entity передаем параметр, который хотим изменить
     */
    void update(Book entity);

    /**
     * Метод удаления записи из файла по id(key)
     *
     * @param id параметр по которому находим строку, которую нужно удалить
     */
    void delete(Integer id);
}