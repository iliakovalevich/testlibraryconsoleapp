package com.intexsoft.services.io;

import com.intexsoft.entities.Book;

import java.util.Map;
import java.util.Scanner;


public class ConsoleService implements IConsoleService {
    static final String FIRST_MENU_ITEM = "1-show all books";
    static final String SECOND_MENU_ITEM = "2-add new book in library";
    static final String THIRD_MENU_ITEM = "3-get book by id";
    static final String EXIT_MENU_ITEM = "0-exit";

    @Override
    public void printString(String str) {
        System.out.println(str);
    }

    @Override
    public Integer getBookId() {
        Scanner sc = new Scanner(System.in);
        System.out.println("Input id book:");
        String choseNumber = sc.next();
        try {
            return Integer.parseInt(choseNumber);
        } catch (NumberFormatException e) {
            System.out.println("Incorrect input");
            return 0;
        }
    }

    @Override
    public String getMenuNumber() {
        Scanner sc = new Scanner(System.in);
        System.out.println(FIRST_MENU_ITEM + "\n" + SECOND_MENU_ITEM + "\n" + THIRD_MENU_ITEM + "\n" + EXIT_MENU_ITEM);
        System.out.println("Chose number from 0 to 3");
        return sc.next();
    }

    @Override
    public void printHashMapValues(Map hashMap) {
        for (Object value : hashMap.values()) {
            System.out.println(value);
        }
    }

    @Override
    public Book createNewBook() {
        Scanner sc = new Scanner(System.in);
        System.out.println("Input book title:");
        String title = sc.next();
        System.out.println("Input book author:");
        String author = sc.next();
        return new Book(title, author);
    }
}
