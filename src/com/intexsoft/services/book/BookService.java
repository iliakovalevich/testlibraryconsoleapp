package com.intexsoft.services.book;

import com.intexsoft.dao.BookFileDao;
import com.intexsoft.dao.IBookFileDao;
import com.intexsoft.entities.Book;

import java.util.HashMap;
import java.util.Map;

public class BookService implements IBookService {
    private final IBookFileDao bookDao = new BookFileDao();

    @Override
    public void create(Book entity) {
        int id = readAll().size() + 1;
        String idBook = Integer.toString(id);
        Book book = new Book(idBook, entity.getTitle(), entity.getAuthor());
        bookDao.create(book);
    }

    @Override
    public Map<String, Book> readAll() {
        Map<String, String> allFileInformationMap;
        Map<String, Book> stringBookMap = new HashMap<>();
        allFileInformationMap = bookDao.readAll();
        for (String key : allFileInformationMap.keySet()) {
            String str = allFileInformationMap.get(key);
            String[] words = str.split(";");
            Book book = new Book(key, words[0], words[1]);
            stringBookMap.put(key, book);
        }
        return stringBookMap;
    }

    @Override
    public void update(Book entity) {

    }

    @Override
    public void delete(Integer id) {

    }

    @Override
    public Book getById(Integer id) {
        Map<String, Book> stringBookMap = readAll();
        return stringBookMap.get(id.toString());
    }
}
