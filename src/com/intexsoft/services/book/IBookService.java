package com.intexsoft.services.book;

import com.intexsoft.entities.Book;

import java.util.Map;

/**
 * Интерфейс для работы с книгами
 *
 * @version 1.0
 */
public interface IBookService {

    /**
     * Метод создания новой книги. Передается объект {@link Book} без id, в методе создается id для объекта
     * и вызывается метод для добаваления сущности в файл
     *
     * @param entity параметр типа Book
     */
    void create(Book entity);

    /**
     * Метод возвращает Map (ключ = id {@link Book}, значение {@link Book}).
     * Возвращает все книги, которые находятся в файле.
     *
     * @return возвращает Map (ключ = id {@link Book}, значение {@link Book})
     */
    Map<String, Book> readAll();

    /**
     * @param entity
     */
    void update(Book entity);

    /**
     * @param id
     */
    void delete(Integer id);

    /**
     * Метод получения книги {@link Book} по id (возвращается информация о книге по заданаму id)
     *
     * @param id передаваемый параметр по которому получаем книгу
     * @return возвращает объект типа Book(книгу) по заданаму параметру(id)
     */
    Book getById(Integer id);
}
